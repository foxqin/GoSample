package service

import (
	"errors"
	connector "webapi/database"
	model "webapi/model"
	"gopkg.in/mgo.v2/bson"
	utility "webapi/utility"
	"gopkg.in/ini.v1")

type Configuration struct {
	DatabaseName string `ini:"databaseName"`
	CollectionName string `ini:"collectionName"`
	EncryptionKey string `ini:"encryptionKey"`
}
	
var configuration = getConfig() 

func getConfig() Configuration{
	var configuration Configuration
	conf, err := ini.Load("web.conf")   //加载配置文件
	if err != nil {
		panic(err)
	}
	conf.BlockMode = false
	err = conf.MapTo(&configuration)   //解析成结构体
	if err != nil {
		panic(err)
	}
	return configuration
}

func getUserByEmail (Email string) (model.UserAccount, error) {
	session := connector.GetSession()
	defer session.Close()
	collection := session.DB(configuration.DatabaseName).C(configuration.CollectionName)
	var userAccount model.UserAccount
	err := collection.Find(bson.M{"email": Email}).One(&userAccount)

	if err != nil {
        panic(err)
	}
	
	return userAccount, err
}

func InsertNewAccount (email string, password string) (error) {
	encryptedPassword, err := utility.Encrypt([]byte(password), []byte(configuration.EncryptionKey))

	if err != nil {
		return err
	}

	session := connector.GetSession()
	defer session.Close()
	collection := session.DB(configuration.DatabaseName).C(configuration.CollectionName)

	userAccount := &model.UserAccount {
		Email : email,
		EncryptedPassword : encryptedPassword,
	}

	err = collection.Insert(userAccount)

	if err != nil {
		return err
	}
	return nil
}

func LoginUser (email string, password string) (error) {
	userAccount, err := getUserByEmail(email)

	if err != nil {
		return err
	}

	decryptedPassword, err := utility.Decrypt([]byte(userAccount.EncryptedPassword), []byte(configuration.EncryptionKey)) 

	if err != nil {
		return err
	}

	if (password == decryptedPassword) {
		return nil
	} else {
		return errors.New("Password is wrong")
	}

	
}