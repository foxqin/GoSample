package dataConnector

import "gopkg.in/mgo.v2"

var (
    mgoSession     *mgo.Session
	MOGODB_URI = "127.0.0.1:27017"
)

func GetSession () *mgo.Session {
    if mgoSession == nil {
        var err error
        mgoSession, err = mgo.Dial(MOGODB_URI)
        if err != nil {
             panic(err) // no, not really
        }
    }
    return mgoSession.Clone()
}