package main

import (
	"github.com/gin-gonic/gin"
	model "webapi/model"
	service "webapi/service"
	"os"
	"io"
	"fmt"
) 


func main() {

	// Disable Console Color, you don't need console color when writing the logs to file.
    gin.DisableConsoleColor()
	
	// Logging to a file.
	f, _ := os.Create("gin.log")
	gin.DefaultWriter = io.MultiWriter(f)

	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	r.POST("/register", registerNewUser)
	r.POST("/login", login)
	r.POST("/upload", upload)
	r.GET("/get", download)
	r.LoadHTMLGlob("html/*")
	r.Static("/assets", "./assets")
	r.GET("/index", loadHTML)
	r.Run(":8080") // listen and serve on 0.0.0.0:8080
}

func registerNewUser(c *gin.Context) {
	result := &model.Result {}

	email := c.PostForm("email")
	password := c.PostForm("password")
	err := service.InsertNewAccount(email, password)

	if err != nil {
		result.Code = -1
		result.Message = err.Error()
		result.Data = false
	} else {
		result.Code = 0
		result.Message = ""
		result.Data = true
	}

	c.JSON(200, result)
}

func login(c *gin.Context) {
	result := &model.Result {}

	email := c.PostForm("email")
	password := c.PostForm("password")
	err := service.LoginUser(email, password)

	if err != nil {
		result.Code = -1
		result.Message = err.Error()
		result.Data = false
	} else {
		result.Code = 0
		result.Message = ""
		result.Data = true
	}

	c.JSON(200, result)
}

func upload(c *gin.Context) {
	// single file
	file, err := c.FormFile("file")
	if err != nil {
		panic(err)
	}

	err = c.SaveUploadedFile(file, file.Filename)

	if err != nil {
		panic(err)
	}

	email := c.PostForm("email")

	// Upload the file to specific dst.
	// c.SaveUploadedFile(file, dst)

	c.String(200, fmt.Sprintf("'%s' uploaded! for user '%s'", file.Filename, email))
	
}

func download(c *gin.Context) {
	
	c.File("FLAMING MOUNTAIN.JPG")
}

func loadHTML(c *gin.Context) {
	c.HTML(200, "index.html", gin.H{})
}