package model

type UserAccount struct {
    Email 		string	`bson:"email"`
	EncryptedPassword string	`bson:"password"`
}